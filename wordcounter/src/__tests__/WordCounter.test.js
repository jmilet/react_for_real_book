import React from 'react';
import { shallow, configure } from 'enzyme';
import WordCounter from '../WordCounter';
import countWords from '../countWords';
import Counter from '../Counter';
import Editor from '../Editor';
import ProgressBar from '../ProgressBar';
import { exportAllDeclaration } from '@babel/types';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('When I type some words', () => {
   const target = 10;
   const inputString = 'One two three four';
   const wordCounter = shallow(<WordCounter targetWordCount={target} />);
   const textArera = wordCounter.find(Editor).dive().find('textarea');

   textArera.simulate('change', {
      target: {
         value: inputString
      }
   });

   it('displays the corret count as a number', () => {
      const counter = wordCounter.find(Counter);
      expect(counter.prop('count')).toBe(countWords(inputString));
   });

   it('displays the correct progress', () => {
      const progressBar = wordCounter.find(ProgressBar);

      expect(progressBar.prop('completion')).toBe(countWords(inputString) / target);
   });
});