import countWords from '../countWords';
import { exportAllDeclaration } from '@babel/types';

describe('the counting function', () => {
   it ('counts the correct number of words', () => {
      expect(countWords('One two trhee')).toBe(3);
   });

   it ('counts an empty string', () => {
      expect(countWords('')).toBe(0);
   });
});