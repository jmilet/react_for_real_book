import React from 'react';
import renderer from 'react-test-renderer';
import Counter from '../Counter';

// npm test -- -u   -> to re-create the UI test snapshot

describe('A counter', () => {
   it('Displays the count and label', () => {
      const counter = renderer.create(<Counter legend="Count" count={35} />);
      expect(counter.toJSON()).toMatchSnapshot();
   });
});