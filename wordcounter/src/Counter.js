import React from 'react';

function Counter({ count }) {
   // React uses className to avoid clashes with the JavaScript class keyword.
   
   return (
      <p className="mb2">
         <label htmlFor="count">Word count: </label>
         <output id="count">
            {count}
         </output>
      </p>
   );
}

export default Counter;