import React from 'react';

function ProgressBar({ completion }) {
   // Also notice that 'htmFor' in place of 'for' in forms.

   const percentage = completion * 100;
   return (
      <div className="mv2 flex flex-column">
         <label htmlFor="progress" className="mv2">
            Progress
         </label>
         <progress value={completion} id="progress" className="bn">
            {percentage} %
         </progress>
      </div>
   );
}

export default ProgressBar;