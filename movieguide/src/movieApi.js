export function requestMovies() {
   return new Promise(resolve =>
      setTimeout(() => resolve([
         { title: 'Rebel without a Case', date: 'Monday' },
         { title: 'Ghost in the Shell', date: 'Tueday' },
         { title: 'High Noon', date: 'Monday' }
      ]), 1000),
   );
}